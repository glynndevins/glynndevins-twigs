/** ========================================================================
 * Standard Mobile Menu
 * This is the basic mobile menu.
 * ======================================================================== */
(function( $ ) {
	/* Mobile Menu */
	$( document ).ready(function() {
		if ($('#mobileNav .nav .movedParent').length === 0) {
			$('#mobileNav').prepend('<a class="navBack" href="#"><span class="glyphicon glyphicon-circle-arrow-left"></span>Back to Navigation</a>');
			$("#mobileNav .nav li.page_item_has_children > a, #mobileNav .nav li.menu-item-has-children > a").each( function() {
				var where = $(this).parents('li').children('ul').children('li:first-child');
				$(this).clone().insertBefore(where).wrap('<li class="movedParent"></li>');
			});
			$('#mobileNav .children, #mobileNav .sub-menu').addClass('bgSecondary');
		}
	});
	$('#mainNavBtn').click(function() {
		$('#mobileNav').toggleClass('open');
		$('#mobileNav .nav').removeClass('slide');
		$('#mobileNav .nav li').removeClass('open');
	});
	$('.navbar-toggle').click(function() {
		$('body').toggleClass('mobileOpen');
	});
	$('#mobileNav .children, #mobileNav .sub-menu').prev().on( 'click', function(d) {
		d.preventDefault();
		$('#mobileNav .nav').addClass('slide');
		$(this).parent().addClass('open');
		$('#mobileNav').addClass('showBack').scrollTop('0px');
	});
	$(function(){
		$('#mobileNav').on('click', '.navBack', function(e) {
			e.preventDefault();
			$('#mobileNav .nav').removeClass('slide');
			$('.page_item_has_children.open, .menu-item-has-children.open').removeClass('open');
			$('#mobileNav').removeClass('showBack');
		});
	});
})( jQuery );