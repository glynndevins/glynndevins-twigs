#GlynnDevins Twigs
This is meant to be used to support child theme builds for all GlynnDevins Platform built websites.

### 0.1.3 - 10/30/18 - Update `failJSHint` flag

* Updating build to not fail on JSHint warnings
* Updating JSHint failure to occur if `--failJSHint` argument was given

### 0.1.2 - 10/16/18 - Update "scripts" task

* Splitting "scripts" task into two: **scripts:dev** and **scripts:build**
* Move the `uglify` method to **scripts:build**

### 0.1.1 - 4/27/2017 - Refine V2 Mobile Menu
* Stop overflow bounce on V2 mobile menu.

### 0.1.0 - 4/27/2017 - V2 Mobile Menu
* Add new mobile menu.
* Add page templates directory to watch.

### 0.0.5 - 4/13/2017 - Versioning Issue

### 0.0.4 - 4/13/2017 - Fix Gulp build
Gulp was building all bower scripts by default. Updated to only build what is specified.

### 0.0.3 - 4/6/2017 - Add Mobile Menu Styles

### 0.0.2 - 4/6/2017 - Update README.md

### 0.0.1 - 4/6/2017 - Initial Build
Includes
* Gulp tasks
* Mobile Menu Javascript
* Home Modules CSS
